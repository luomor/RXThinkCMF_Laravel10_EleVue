## 📚 项目介绍
一款 PHP 语言基于 Laravel10.x、Vue2.x、ElementUI、MySQL等框架精心打造的一款模块化、插件化、高性能的前后端分离架构敏捷开发框架，可用于快速搭建前后端分离后台管理系统，本着简化开发、提升开发效率的初衷，目前框架已集成了完整的RBAC权限架构和常规基础模块，前端Vue端支持多主题切换，可以根据自己喜欢的风格选择想一个的主题，实现了个性化呈现的需求；

为了敏捷快速开发，提升研发效率，框架内置了一键CRUD代码生成器，自定义了模块生成模板，包括后端PHP文件模块和前端Vue端个性化模板，可以根据已建好的表结构(字段注释需规范)快速的一键生成整个模块的所有代码和增删改查等等功能业务，真正实现了低代码开发，极大的节省了人力成本的同时提高了开发效率，缩短了研发周期，是一款真正意义上实现组件化、低代码敏捷开发框架。

## 🍻 环境要求:

* PHP >= 8.1（推荐）
* PDO PHP Extension
* MBstring PHP Extension
* CURL PHP Extension
* 开启静态重写
* 要求环境支持pathinfo

## 🍪 内置模块
+ 用户管理：用于维护管理系统的用户，常规信息的维护与账号设置。
+ 角色管理：角色菜单管理与权限分配、设置角色所拥有的菜单权限。
+ 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
+ 职级管理：主要管理用户担任的职级。
+ 岗位管理：主要管理用户担任的岗位。
+ 部门管理：主要管理系统组织架构，对组织架构进行统一管理维护。
+ 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
+ 登录日志：系统登录日志记录查询包含登录异常。
+ 字典管理：对系统中常用的较为固定的数据进行统一维护。
+ 配置管理：对系统的常规配置信息进行维护，网站配置管理功能进行统一维护。
+ 城市管理：统一对全国行政区划进行维护，对其他模块提供行政区划数据支撑。
+ 友链管理：对系统友情链接、合作伙伴等相关外链进行集成维护管理的模块。
+ 个人中心：主要是对当前登录用户的个人信息进行便捷修改的功能。
+ 广告管理：主要对各终端的广告数据进行管理维护。
+ 站点栏目：主要对大型系统网站等栏目进行划分和维护的模块。
+ 会员管理：对各终端注册的会员进行统一的查询与管理的模块。
+ 网站配置：对配置管理模块的数据源动态解析与统一维护管理的模块。
+ 通知公告：系统通知公告信息发布维护。
+ 代码生成：一键生成模块CRUD的功能，包括后端和前端Vue等相关代码。
+ 案例演示：常规代码生成器一键生成后的演示案例。

## 👷 软件信息

* 软件名称：RXThinkCMF敏捷开发框架Laravel10.x+EleVue版本
* 软件作者：@牧羊人 团队荣誉出品
* 软件出处：南京RXThinkCMF研发中心
* 软件协议：LGPL-3.0
* 官网网址：[https://www.rxthink.cn](https://www.rxthink.cn)
* 文档网址：[http://docs.laravel10.elevue.rxthink.cn](http://docs.laravel10.elevue.rxthink.cn)

## 🎨 系统演示

+ 演示地址：[http://manage.laravel10.elevue.rxthink.cn](http://manage.laravel10.elevue.rxthink.cn)

账号 | 密码| 操作权限
---|---|---
admin | 123456| 演示环境无法进行修改删除操作

## 📌 版本说明

版本名称 | 版本说明 | 版本地址
---|---|---
ThinkPhp3.2+Layui混编版 | 采用ThinkPhp3.2、Layui、MySQL等框架研发的混编专业版本   | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp3.2_Layui
ThinkPhp5.1+Layui混编版 | 采用ThinkPhp5.1、Layui、MySQL等框架研发的混编专业版本   | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp5.1_Layui
ThinkPhp6.x+Layui混编版 | 采用ThinkPhp6、Layui、MySQL等框架研发的混编专业版本     | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp6_Layui
ThinkPhp8.x+Layui混编版 | 采用ThinkPhp8、Layui、MySQL等框架研发的混编专业版本     | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp8_Layui
Laravel8.x+Layui混编版 | 采用Laravel8、Layui、MySQL等框架研发的混编专业版本      | https://gitee.com/rxthinkcmf/RXThinkCMF_laravel8_Layui
Laravel9.x+Layui混编版 | 采用Laravel9、Layui、MySQL等框架研发的混编专业版本      | https://gitee.com/rxthinkcmf/RXThinkCMF_Laravel9_Layui
Laravel10.x+Layui混编版 | 采用Laravel10、Layui、MySQL等框架研发的混编专业版本     | https://gitee.com/rxthinkcmf/RXThinkCMF_Laravel10_Layui
ThinkPhp3.2+EleVue前后端分离版 | 采用ThinkPhp3.2、Vue2.x、ElementUI等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp3.2_EleVue
ThinkPhp3.2+AntdVue前后端分离版 | 采用ThinkPhp3.2、Vue3.x、AntDesign等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp3.2_AntdVue
ThinkPhp5.1+EleVue前后端分离版 | 采用ThinkPhp5.1、Vue2.x、ElementUI等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp5.1_EleVue
ThinkPhp5.1+AntdVue前后端分离版 | 采用ThinkPhp5.1、Vue2.x、AntDesign等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp5.1_AntdVue
ThinkPhp6.x+EleVue前后端分离版 | 采用ThinkPhp6、Vue2.x、ElementUI等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp6_EleVue
ThinkPhp6.x+AntdVue前后端分离版 | 采用ThinkPhp6、Vue3.x、AntDesign等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp6_AntdVue
ThinkPhp8.x+EleVue前后端分离版 | 采用ThinkPhp8、Vue2.x、ElementUI等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp8_EleVue
ThinkPhp8.x+AntdVue前后端分离版 | 采用ThinkPhp8、Vue3.x、AntDesign等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_ThinkPhp8_AntdVue
Laravel8.x+EleVue前后端分离版 | 采用Laravel8、Vue2.x、ElementUI等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_Laravel8_EleVue
Laravel8.x+AntdVue前后端分离版 | 采用Laravel8、Vue3.x、AntDesign等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_Laravel8_AntdVue
Laravel9.x+EleVue前后端分离版 | 采用Laravel9、Vue2.x、ElementUI等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_Laravel9_EleVue
Laravel9.x+AntdVue前后端分离版 | 采用Laravel9、Vue3.x、AntDesign等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_Laravel9_AntdVue
Laravel10.x+EleVue前后端分离版 | 采用Laravel10、Vue2.x、ElementUI等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_Laravel10_EleVue
Laravel10.x+AntdVue前后端分离版 | 采用Laravel10、Vue3.x、AntDesign等框架研发前后端分离版本 | https://gitee.com/rxthinkcmf/RXThinkCMF_Laravel10_AntdVue

## 🔧 模块展示

![效果图](./public/uploads/demo/1.png)

![效果图](./public/uploads/demo/2.png)

![效果图](./public/uploads/demo/3.png)

![效果图](./public/uploads/demo/4.png)

![效果图](./public/uploads/demo/5.png)

![效果图](./public/uploads/demo/6.png)

![效果图](./public/uploads/demo/7.png)

![效果图](./public/uploads/demo/8.png)

![效果图](./public/uploads/demo/9.png)

![效果图](./public/uploads/demo/10.png)

![效果图](./public/uploads/demo/11.png)

![效果图](./public/uploads/demo/12.png)

![效果图](./public/uploads/demo/13.png)

![效果图](./public/uploads/demo/14.png)

![效果图](./public/uploads/demo/15.png)

![效果图](./public/uploads/demo/16.png)

![效果图](./public/uploads/demo/17.png)

![效果图](./public/uploads/demo/18.png)

![效果图](./public/uploads/demo/19.png)

![效果图](./public/uploads/demo/20.png)

![效果图](./public/uploads/demo/21.png)

![效果图](./public/uploads/demo/22.png)

![效果图](./public/uploads/demo/23.png)

![效果图](./public/uploads/demo/24.png)

## ✨  特别鸣谢
感谢[Laravel](https://laravel.com/)、[Vue](https://cn.vuejs.org/)、[ElementUI](https://element.eleme.cn/#/zh-CN)等优秀开源项目。

## 📚 版权信息

软件版权和最终解释权归软件框架研发团队所有，商业版使用需授权，未授权禁止恶意传播和用于商业用途，否则将追究相关人的法律责任。

本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2018~2023 [rxthink.cn](https://www.rxthink.cn) All rights reserved。

更多细节参阅 [LICENSE](LICENSE)
