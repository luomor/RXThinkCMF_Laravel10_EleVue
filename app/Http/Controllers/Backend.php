<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Http\Controllers;

use App\Helpers\JwtUtils;
use App\Models\UserModel;

/**
 * 后台控制器
 * @author 牧羊人
 * @date: 2023/3/28 12:04
 */
class Backend extends BaseController
{
    // 模型
    protected $model;
    // 服务
    protected $service;
    // 校验
    protected $validate;
    // 登录ID
    protected $userId;
    // 登录信息
    protected $userInfo;

    /**
     * 构造函数
     */
    public function __construct()
    {
        parent::__construct();

        // 初始化配置
        $this->initConfig();

        // 登录检测中间件
        $this->middleware('user.login');

        // 权限检测中间件
        $this->middleware('check.auth');

        // 初始化登录信息
        $this->middleware(function ($request, $next) {
            $userId = JwtUtils::getUserId();

            // 登录验证
            $this->initLogin($userId);

            return $next($request);
        });
    }

    /**
     * 初始化配置
     * @return void
     * @author 牧羊人
     * @date: 2023/3/28 12:06
     */
    public function initConfig()
    {
        // 请求参数
        $this->param = \request()->input();

        // 分页基础默认值
        defined('PERPAGE') or define('PERPAGE', isset($this->param['limit']) ? $this->param['limit'] : 20);
        defined('PAGE') or define('PAGE', isset($this->param['page']) ? $this->param['page'] : 1);
    }

    /**
     * 登录验证
     * @param $userId 用户ID
     * @return void
     * @author 牧羊人
     * @date: 2023/3/28 12:06
     */
    public function initLogin($userId)
    {
        // 登录用户ID
        $this->userId = $userId;

        // 登录用户信息
        if ($userId) {
            $adminModel = new UserModel();
            $userInfo = $adminModel->getInfo($this->userId);
            $this->userInfo = $userInfo;
        }
    }

    /**
     * 获取数据列表
     * @return mixed
     * @author 牧羊人
     * @date: 2023/3/28 12:05
     */
    public function index()
    {
        $result = $this->service->getList();
        return $result;
    }

    /**
     * 获取数据详情
     * @return mixed
     * @author 牧羊人
     * @date: 2023/3/28 12:05
     */
    public function info()
    {
        $result = $this->service->info();
        return $result;
    }

    /**
     * 添加或编辑
     * @return mixed
     * @author 牧羊人
     * @date: 2023/3/28 12:05
     */
    public function edit()
    {
        $result = $this->service->edit();
        return $result;
    }

    /**
     * 删除数据
     * @return mixed
     * @author 牧羊人
     * @date: 2023/3/28 12:05
     */
    public function delete()
    {
        $result = $this->service->delete();
        return $result;
    }

    /**
     * 设置状态
     * @return mixed
     * @author 牧羊人
     * @date: 2023/3/28 12:05
     */
    public function status()
    {
        $result = $this->service->status();
        return $result;
    }

    /**
     * 批量删除
     * @return array|void
     * @author 牧羊人
     * @date: 2023/3/28 12:05
     */
    public function batchDelete()
    {
        if (IS_POST) {
            $ids = explode(',', $_POST['id']);
            //批量删除
            $num = 0;
            foreach ($ids as $key => $val) {
                $res = $this->model->drop($val);
                if ($res !== false) {
                    $num++;
                }
            }
            return message('本次共选择' . count($ids) . "个条数据,删除" . $num . "个");
        }
    }
}
