<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to your application's "home" route.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     */
    public function boot(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));

            // 自定加载路由
            $this->mapAllRoutes();
        });
    }

    /**
     * 自定义加载某个目录下的全部路由
     * @return void
     * @author 牧羊人
     * @date: 2023/3/28 17:45
     */
    protected function mapAllRoutes()
    {
        $all_route_file = scandir(base_path("routes/web"));
        foreach ($all_route_file as $val) {
            if (strstr($val, ".php")) {
                Route::middleware("web")
                    ->namespace($this->namespace)
                    ->group(base_path("routes/web/" . $val));
            }
        }
    }
}
