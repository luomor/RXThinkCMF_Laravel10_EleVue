<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Services;

use App\Models\AdminModel;
use App\Models\AdminRomModel;
use App\Models\MenuModel;

/**
 * 权限管理-服务类
 * @author 牧羊人
 * @date: 2023/3/28 13:41
 */
class AdminRomService extends BaseService
{
    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->model = new AdminRomModel();
    }

    /**
     * 查询数据列表
     * @return array 返回结果
     * @author 牧羊人
     * @date: 2023/3/28 13:43
     */
    public function getList()
    {
        // 类型
        $type = request()->input("type");
        // 类型ID
        $typeId = request()->input('typeId');

        // 获取所有菜单
        $menuModel = new MenuModel();
        $menuList = $menuModel->getList([], [['sort', 'asc']]);
        // 获取有权限的菜单
        $where = [
            ['type', '=', $type],
            ['type_id', '=', $typeId],
        ];
        $adminRomModel = new AdminRomModel();
        $permissionList = $adminRomModel->getList($where, [['menu_id', 'asc']]);
        $checkList = [];
        if ($permissionList) {
            $checkList = array_column($permissionList, "menu_id");
        }
        $list = [];
        if (!empty($menuList)) {
            foreach ($menuList as $val) {
                $data = [];
                $data['id'] = $val['id'];
                $data['name'] = trim($val['name']);
                $data['pId'] = $val['pid'];
                if (in_array($val['id'], $checkList)) {
                    $data['checked'] = true;
                } else {
                    $data['checked'] = false;
                }
                $data['open'] = true;
                $list[] = $data;
            }
        }
        return message("操作成功", true, $list);
    }

    /**
     * 保存权限数据
     * @return array 返回结果
     * @author 牧羊人
     * @date: 2023/3/28 13:47
     */
    public function setPermission()
    {
        // 参数
        $param = request()->all();
        // 类型
        $type = intval($param['type']);
        // 类型ID
        $typeId = intval($param['typeId']);
        if (!$typeId) {
            return message("类型ID不能为空", false);
        }
        // 删除现有的权限
        $where = [
            ['type', '=', $type],
            ['type_id', '=', $typeId],
        ];
        $adminRomModel = new AdminRomModel();
        $permissionList = $adminRomModel->getList($where, [['menu_id', 'asc']]);
        if ($permissionList) {
            $itemList = array_column($permissionList, "id");
            $adminRomModel->deleteAll($itemList, true);
        }
        // 权限ID
        $authIds = trim($param['authIds']);
        if ($authIds) {
            $itemArr = explode(',', $authIds);
            foreach ($itemArr as $val) {
                $data = [
                    'type' => $type,
                    'type_id' => $typeId,
                    'menu_id' => $val,
                ];
                $adminRomModel = new AdminRomModel();
                $adminRomModel->edit($data);
            }
        }
        return message("操作成功");
    }

    /**
     * 获取权限数据
     * @param $adminId 用户ID
     * @return array|mixed 返回结果
     * @author 牧羊人
     * @date: 2023/3/28 13:48
     */
    public function getPermissionList($adminId)
    {
        if ($adminId == 1) {
            // 管理员
            $menuModel = new MenuModel();
            $menuList = $menuModel->getChilds(0);
            return $menuList;
        } else {
            // 非管理员
            $adminModel = new AdminModel();
            $adminInfo = $adminModel->getInfo($adminId);
            $menuList = $this->model->getPermissionMenu($adminInfo['role_ids'], $adminId, 1, 0);
            return $menuList;

        }
    }

    /**
     * 获取权限节点数据
     * @param $adminId 用户ID
     * @return array 返回结果
     * @author 牧羊人
     * @date: 2023/3/28 13:48
     */
    public function getPermissionFuncList($adminId)
    {
        if ($adminId == 1) {
            // 管理员
            $menuModel = new MenuModel();
            $menuList = $menuModel->where("type", 4)
                ->where("mark", 1)
                ->get('permission')
                ->toArray();
            $permissionList = array_key_value($menuList, "permission");
            return $permissionList;
        } else {
            // 非管理员
            $adminModel = new AdminModel();
            $adminInfo = $adminModel->getInfo($adminId);
            $permissionList = $this->model->getPermissionFuncList($adminInfo['role_ids'], $adminId);
            return $permissionList;
        }
    }
}
