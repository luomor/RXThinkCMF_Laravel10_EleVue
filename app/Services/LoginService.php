<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace App\Services;

use App\Helpers\Jwt;
use App\Helpers\JwtUtils;
use App\Models\ActionLogModel;
use App\Models\UserModel;
use Carbon\Carbon;
use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

/**
 * 登录-服务类
 * @author 牧羊人
 * @date: 2023/3/28 11:40
 */
class LoginService extends BaseService
{
    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->model = new UserModel();
    }

    /**
     * 获取验证码
     * @return array
     * @author 牧羊人
     * @date: 2023/3/28 11:47
     */
    public function captcha()
    {
        $phrase = new PhraseBuilder;
        // 设置验证码位数
        $code = $phrase->build(4);
        // 生成验证码图片的Builder对象，配置相应属性
        $builder = new CaptchaBuilder($code, $phrase);
        // 设置背景颜色25,25,112
        $builder->setBackgroundColor(255, 255, 255);
        // 设置倾斜角度
        $builder->setMaxAngle(25);
        // 设置验证码后面最大行数
        $builder->setMaxBehindLines(10);
        // 设置验证码前面最大行数
        $builder->setMaxFrontLines(10);
        // 设置验证码颜色
        $builder->setTextColor(230, 81, 175);
        // 可以设置图片宽高及字体
        $builder->build($width = 165, $height = 45, $font = null);
        // 获取验证码的内容
        $phrase = $builder->getPhrase();
        // 把内容存入 cache，10分钟后过期
        $key = Uuid::uuid1()->toString();
        $this->model->setCache($key, $phrase, Carbon::now()->addMinutes(10));
        // 组装接口数据
        $data = [
            'key' => $key,
            'captcha' => $builder->inline(),
        ];
        return message("操作成功", true, $data);
    }

    /**
     * 系统登录
     * @return array
     * @author 牧羊人
     * @date: 2023/3/28 11:47
     */
    public function login()
    {
        // 参数
        $param = request()->all();
        // 用户名
        $username = trim($param['username']);
        // 密码
        $password = trim($param['password']);
        // 验证规则
        $rules = [
            'username' => 'required|min:2|max:20',
            'password' => 'required|min:6|max:20',
            'captcha' => ['required'],
        ];
        // 规则描述
        $messages = [
            'required' => ':attribute为必填项',
            'min' => ':attribute长度不符合要求',
            'captcha.required' => '验证码不能为空',
        ];
        // 验证
        $validator = Validator::make($param, $rules, $messages, [
            'username' => '用户名称',
            'password' => '登录密码'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            foreach ($errors as $key => $value) {
                return message($value[0], false);
            }
        }
        // 验证码校验
        $key = isset($param['key']) ? trim($param['key']) : "";
        $captcha = $this->model->getCache($key);
        if (strtolower($captcha) != strtolower($param['captcha'])) {
            return message("请输入正确的验证码", false);
        }

        // 用户验证
        $info = $this->model->getOne([
            ['username', '=', $username],
        ]);
        if (!$info) {
            return message('您的登录用户名不存在', false);
        }
        // 密码校验
        $password = get_password($password . $username);
        if ($password != $info['password']) {
            return message("您的登录密码不正确", false);
        }
        // 使用状态校验
        if ($info['status'] != 1) {
            return message("您的帐号已被禁用", false);
        }

        // 设置日志标题
        ActionLogModel::setTitle("登录系统");
        ActionLogModel::setUsername($info['username']);
        ActionLogModel::record();

        // JWT生成token
        $jwt = new Jwt();
        $token = $jwt->getToken($info['id']);

        // 结果返回
        $result = [
            'access_token' => $token,
        ];
        return message('登录成功', true, $result);
    }

    /**
     * 退出系统
     * @return array
     * @author 牧羊人
     * @date: 2023/3/28 11:47
     */
    public function logout()
    {
        $userId = JwtUtils::getUserId();
        $userInfo = $this->model->getInfo($userId);
        // 记录退出日志
        ActionLogModel::setTitle("注销系统");
        ActionLogModel::setUsername(isset($userInfo['username']) ? $userInfo['username'] : "");
        // 创建退出日志
        ActionLogModel::record();
        return message();
    }
}
